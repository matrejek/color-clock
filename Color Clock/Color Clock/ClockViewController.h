//
//  ViewController.h
//  Color Clock
//
//  Created by Matrejek, Mateusz on 16/12/14.
//  Copyright (c) 2014 Mateusz Matrejek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClockViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *colorLabel;

@end
